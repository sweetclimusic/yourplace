/// Copyright (c) 2019 Green Farm Games Ltd.
///
/// All rights reserved
/// Author: Ashlee Muscroft, 30/04/2019
/// This might not be miraculous code, but I will learn swift and iOS through it.

import Foundation
import UIKit
import ARKit

class PictureFrameSettings {
  
  let portrait = UIImage(named: "canvas-thumb");
  let longPortrait = UIImage(named: "long-canvas-thumb");
  
  let pictureFrames = [
    UIImage(named: "gfg-logo"),
    UIImage(named: "black-ring"),
    UIImage(named: "blue-splodge"),
    UIImage(named: "three-stroke"),
    ]
  let pictureSizes = [
    "Portrait",
    "Landscape",
    "Narrow Portrait",
  ]
  let pictureNames = [
    "Green Farm Games",
    "Ring",
    "Rising Tide",
    "Three Ships",
    ]
  let PictureDescriptions = [
    "Vector Arkwork, designed by the developer himself. Show your support by rebranding your space with this iconic logo.",
    "Never broken always one, the ring artwork. This print done in black ink brings life to any room.",
    "Rising tide carrys all ships, this raging use of blue hues makes for a surreal and tranquil piece.",
    "Three ships at port, the artist's Liam Goah reimagined his view of the harbour from the bedroom of his childhood home."
    ]
  // simulate a min bounding box in Z axis
  let pictureOffsets = [
    SCNVector3(0, 0, 0),
    SCNVector3(0, 0.32, 0),
    SCNVector3(0, 0, 0),
    SCNVector3(0, 0, 0),
    ]
  var baseMaterial: SCNMaterial = SCNMaterial()
  
  var currentImage = 0
  var selectedSize = 0
  // Default to the main app logo
  var framedImage = UIImage(named: "gfg-logo");
  
  func currentPicturePiece() -> SCNNode {
    switch currentImage {
    case 0:
      return createPortrait(pictureImage: framedImage)
    case 1:
      return createLandscape(pictureImage: framedImage)
    case 2:
      return createNarrowPortrait(pictureImage: framedImage)
    case 3:
      return createPanarama(pictureImage: framedImage)
    default:
      return createPortrait(pictureImage: framedImage)
    }
  }
  
  func currentPictureOffset() -> SCNVector3 {
    return pictureOffsets[currentImage]
  }
  
  // Function to multiplay the base canvas texture with the selected image texture
  //returns a new texture for the currently selected scnnode.
  func setCanvasPicture(selectedCanvasImage: UIImage, uvChannel: Int? = 0) -> SCNMaterial {
    //material itself isn't mutated, but SCNMaterialProperty is with multiply if we have a baseTexture
    let mulSCNMaterial = SCNMaterial()
    mulSCNMaterial.diffuse.contents = selectedCanvasImage
    if let selectUVChannel = uvChannel {
      mulSCNMaterial.diffuse.mappingChannel = selectUVChannel
    }
    return mulSCNMaterial
  }
  /**
   Wrapper for creating a picture canvas scene object
   - Parameter pictureImage: optional diffuse texture to apply to the base material of the canvas photo.
   - Parameter nodeName: sceneKitObject to add to the scene as a child node.
   - Returns: node, the newly created node for the scene.
 **/
  fileprivate func generateSCNNode(_ pictureImage: UIImage?, nodeName: String) -> SCNNode {
    let scene = SCNScene(named: "SceneAssets.scnassets/pictures.scn")
    let picture = pictureImage ?? UIImage(named: "gfg-logo")
    // It is known. this isn't a dynamic canvas.
    // material 1 is the "painting" material. Replace that one!
    let matIndex = 1;
    // painting UVChannel is at channel 1 in pictures.scn, It is known.
    let uvChannel = 1;
    // Replace the painting material
    if let node = scene?.rootNode.childNode(withName: nodeName, recursively: true) {
      node.geometry?.replaceMaterial(at: matIndex, with: setCanvasPicture(selectedCanvasImage: picture!,uvChannel: uvChannel) )
      return node
    }
    let canvasNode = scene?.rootNode.childNode(withName: "canvas", recursively: false)
    return canvasNode!
  }
  
  func createPortrait(pictureImage: UIImage?) -> SCNNode {
    return generateSCNNode(pictureImage, nodeName: "canvas")
  }
  
  func createLandscape(pictureImage: UIImage?) -> SCNNode {
    return generateSCNNode(pictureImage, nodeName: "long_canvas")
  }
  
  func createNarrowPortrait(pictureImage: UIImage?) -> SCNNode {
    return generateSCNNode(pictureImage, nodeName: "long_canvas")
  }
  
  func createPanarama(pictureImage: UIImage?) -> SCNNode {
    return generateSCNNode(pictureImage, nodeName: "long_canvas")
  }
  
  func generateBaseMaterial() -> SCNMaterial {
    let baseMaterial = SCNMaterial()
    baseMaterial.isDoubleSided = true
    baseMaterial.diffuse.contents = UIImage(named: "canvas-albedo")
    baseMaterial.ambientOcclusion.contents = UIImage(named: "canvas-ao")
    baseMaterial.normal.contents = UIImage(named: "canvas-normal")
    baseMaterial.displacement.contents = UIImage(named: "canvas-displacement")
    baseMaterial.specular.contents = UIImage(named: "canvas-specular")
    return baseMaterial
  }
  
}

