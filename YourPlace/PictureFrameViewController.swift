/// Copyright (c) 2019 Green Farm Games Ltd.
///
/// All rights reserved
/// Author: Ashlee Muscroft, 5/05/2019
/// This might not be miraculous code, but I will learn swift and iOS through it.

import UIKit

class PictureFrameViewController: UIViewController {

  @IBOutlet var pictureFrameCollectionView: UICollectionView!
  var pictureFrameSettings: PictureFrameSettings!
    //what is this coder in the init doing?
    required init?(coder aDecoder: NSCoder) {
      super.init(coder: aDecoder)
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view. Bind view as the dataSource and the delegate(target) for the action when a user clicks the picture frame icon
      let customTabBarController = self.tabBarController as! CustomTabBarController
      pictureFrameSettings = customTabBarController.pictureFrameSettings
      pictureFrameCollectionView.delegate = self
      pictureFrameCollectionView.dataSource = self
      // Base material set in picture.scn
      // pictureFrameSettings.baseMaterial = pictureFrameSettings.generateBaseMaterial()
    }
  }

//Extend the pictureFrame class to become a collection with picture frames and art
extension PictureFrameViewController : UICollectionViewDelegate {
  
  func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    pictureFrameSettings.currentImage = indexPath.row
    let sourcedImage: UIImage = pictureFrameSettings.pictureFrames[indexPath.row] ?? UIImage(named: "gfg-logo")!
    pictureFrameSettings.framedImage = sourcedImage
    tabBarController?.selectedIndex = 0
  }
}

extension PictureFrameViewController : UICollectionViewDataSource {
  //what pictureFrame we have selected. populate the display information in the ViewController.
  //create a reusable cell, (internally mangaged?) but replace the contents of the cell
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    
    let cell = pictureFrameCollectionView.dequeueReusableCell(withReuseIdentifier: "pictureFrameCollectionViewCell", for: indexPath)
    //configure picture
    let pictureFramePic = cell.viewWithTag(2000) as! UIImageView
    pictureFramePic.image = pictureFrameSettings.pictureFrames[indexPath.row]
    //pickerview to select various canvas sizes, default to first
    let sizePickerView = cell.viewWithTag(2001) as! UIPickerView
    sizePickerView.delegate = self
    sizePickerView.dataSource = self
    //configure label
    let nameLabel = cell.viewWithTag(2002) as! UILabel
    nameLabel.text = pictureFrameSettings.pictureNames[indexPath.row]
    //configure description
    let descriptionLabel = cell.viewWithTag(2003) as! UITextView
    descriptionLabel.isEditable = false
    descriptionLabel.text = pictureFrameSettings.PictureDescriptions[indexPath.row]
    descriptionLabel.isScrollEnabled = true
    
    return cell
  }
  
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return pictureFrameSettings.pictureNames.count
  }
}

extension PictureFrameViewController: UIPickerViewDelegate, UIPickerViewDataSource{
  //required implementations for UIPickerViewDataSource
  func numberOfComponents(in pickerView: UIPickerView) -> Int {
    return 1
  }
  func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
    return pictureFrameSettings.pictureSizes.count
  }
  //required implementations for UIPickerViewDelegate
  func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
    pickerView.layer.borderWidth = 1
    pickerView.layer.borderColor = UIColor.darkText.cgColor
    return pictureFrameSettings.pictureSizes[row]
  }
  func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
    pictureFrameSettings.selectedSize = row
  }
}
